<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<?php
    session_start();
    if($_SESSION['logged_in'] == false) {
        header('location:login.php');
    }
?>